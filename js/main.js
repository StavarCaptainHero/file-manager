// ajax 

$(document).ready(function() {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: 'https://next.json-generator.com/api/json/get/Ek78KDIxu',
        success: function(data) {
            var base = data;
            console.log(base);
            sortArray(base);
            generateList(base);
            onAddFolder(base);
            onAddFile(base);
            onDelete();
            onEdit(base);
            disableModal();

            setInterval(function(){

                sortArray(base);
                
            }, 500);

        }    
    });
});

// variables
var filesViewer = document.querySelector('.content'),
    mapList = document.querySelector('.map_list'),
    folderImg = "img/folder.png",
    fileImg = "img/writing.png",
    editImg = "img/pen.png",
    deleteImg = "img/stop.png";

// functions

function sortArray(base) {

    base.sort(function(a, b){

        if (a.title > b.title) {

            return 1;
            
        }

        if (b.title > a.title) {

            return -1;

        }

        return 0;

    });
    
}

function generateList(base, map, file) {

    this.map = map;

    this.file = file;

    generateMap (base, map, file);

    onFolderHover();

    onFileHover();

    onArrowClick();

}

function generateMap (base, map, file) {

    for (var i = 0; i < base.length; i++) {

        map = `<li class="folder disabled">
                <div class="folder_content">
                    <div class="description">
                    <div class="arrow_content">
                        <div class="drop_arrow"></div>
                    </div>
                    <img src="${folderImg}">
                    <h3 class="title">${base[i].title}</h3>
                    <i>(${base[i].type})</i>
                    </div>
                </div>
                    <ul class="folder_list"></ul>
                </li>`

        if (base[i].type === "folder") {

            $(map).appendTo(mapList);

        }
    }

    generateFile(base, file);

    generateButtons();

}

function generateFile(base, file) {

    for (var i = 0; i < base.length; i++) {

        file = `<li class="file">
                <img src="${fileImg}">
                    <p class="title">${base[i].title}</p>
                    <i>(${base[i].type})</i>
                </li>`

        if (base[i].type === "file") {

            $(file).appendTo($('.folder_list'));

        }
    }
}

function generateButtons() {

    var buttons = `<div class="buttons_group disabled">
                        <button class="btn edit-btn">
                            <img src="${editImg}">
                        </button>
                        <button class="btn delete-btn">
                            <img src="${deleteImg}">
                        </button>
                    </div>`

    $(buttons).appendTo('.folder_content') && $(buttons).appendTo('.folder_list li');
    
}

function generateModal() {

    var modal = `<div class="edit_modal modal">
                    <form id="modal_form">
                        <img src="${deleteImg}" class="close_modal">
                        <input type="text" id="name">
                        <input type="button" value="Submit" id="submit_modal">
                    </form>
                </div>`

    $(modal).appendTo($('main'));

}

// listeners

function onFolderHover() {

    $('.folder').on('mouseenter', function(){

        $(this).addClass('active');

        $(this).removeClass('disabled');
        
        $(this).find($('.buttons_group')).removeClass('disabled');

    });

    $('.folder').on('mouseleave', function(){

        $(this).removeClass('active');

        $(this).addClass('disabled');

        $(this).find($('.buttons_group')).addClass('disabled');

    });
}

function onFileHover() {

    $('.file').on('mouseenter', function(){

        $(this).addClass('active');
        
        $(this).find($('.buttons_group')).removeClass('disabled');

    });

    $('.file').on('mouseleave', function(){

        $(this).removeClass('active');

        $(this).find($('.buttons_group')).addClass('disabled');

    });
}

function onArrowClick() {

    $('.arrow_content').on('click', function(){

        $(this).find($('.drop_arrow')).toggleClass('active_arrow');

        $('.active').find($('.folder_list')).toggleClass('opened');


        if ($('.drop_arrow').hasClass('active_arrow') && $('.folder_list').hasClass('opened')) {
    
                $('.folder_list.opened').parent().addClass('active');
    
                $('.folder.active').on('mouseleave', function(){
    
                    $(this).removeClass('disabled');
    
                    $(this).addClass('active');
        
                });
    
        } else {
            $('.folder').removeClass('active');

            $('.folder').addClass('disabled');

            $('.folder').on('mouseleave', function(){

                $('.folder').addClass('disabled');

                $('.folder').removeClass('active');
    
            });
        }
    });
}

function onEdit(base) {

    $('.edit-btn').on('click', function(){

        $(this).parent().parent().parent().addClass('active');

        $('.folder.active').on('mouseleave', function(){

            $(this).addClass('active');

        });

        generateModal();

        closeModal();

        $('#submit_modal').on('click', function(){

            var newName = $('#name').val();

            if ($('.folder').hasClass('active')) {

                var newTitle = $('.active').find($('h3.title'));
                
                newTitle.text(newName);

            }

            if($('.folder').hasClass('active')) {

                $('.folder').removeClass('active');

                $('.folder').on('mouseleave', function(){

                    $('.folder').removeClass('active');
        
                });
                
            }

        })
        
    });
}

function onDelete() {

    $('.folder_content .delete-btn').on('click', function(){

        if($('.folder').hasClass('active')){

            $('.folder.active').addClass('deleted');

        }
     });

     $('.folder_list .delete-btn').on('click', function(){

        if ($('.file').hasClass('active')) {

            $('.file.active').addClass('deleted');

        }
    });
}

function onAddFolder(base) {

    $('.add_map').on('click', function(){

        generateModal();

        closeModal();

        $('#submit_modal').on('click', function(){


            var newName = $('#name').val();

            var type = "folder";

            var newFolder = `<li class="folder disabled">
                            <div class="folder_content">
                                <div class="description">
                                <div class="arrow_content">
                                    <div class="drop_arrow"></div>
                                </div>
                                <img src="${folderImg}">
                                <h3 class="title">${newName}</h3>
                                <i>(${type})</i>
                                </div>
                            </div>
                                <ul class="folder_list"></ul>
                            </li>`
                            
            if ($('.drop_arrow').hasClass('active_arrow')) {

                $(newFolder).appendTo($('.active .opened'));
                
            } else {

                $(newFolder).appendTo('.map_list');

            }

            var newBase = base.push({type, title: newName});

            base.splice(newBase);

        });
    });
}

function onAddFile(base) {

    $('.add_file').on('click', function(){

        generateModal();

        closeModal();

        $('#submit_modal').on('click', function(){

            var newName = $('#name').val();

            var type = "file";

            var newFile = `<li>
                            <img src="${fileImg}">
                                <p class="title">${newName}</p>
                                <i>(${type})</i>
                            </li>`
                            
            if ($('.drop_arrow').hasClass('active_arrow')) {

                $(newFile).appendTo($('.active .opened'));

            } else {

                $(newFile).appendTo('.map_list');

            }
            var newBase = base.push({type, title: newName});

            base.splice(newBase);

        });

    });
}

function closeModal() {
    $('.close_modal').on('click', function(){

        $('.modal').remove();

    });
}

function disableModal() {

    $("#form").on('submit', function(e) {
        
        return false;

    });
}